package scripts.fc.fcaiominer;

import java.awt.Graphics;

import org.tribot.api.General;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Pausing;
import org.tribot.script.interfaces.Starting;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.fcaiominer.gui.GUI;
import scripts.fc.fcaiominer.tasks.MinerTaskManager;
import scripts.fc.fcaiominer.utils.Utils;
import scripts.fc.fcpaint.FCPaint;
import scripts.fc.fcpaint.FCPaintable;
import scripts.fc.framework.Task;
import scripts.fc.utils.FCInventoryListener;
import scripts.fc.utils.FCInventoryObserver;

@ScriptManifest(
		authors     = { 
		    "Final Calibur",
		}, 
		category    = "Mining", 
		name        = "FC AIO Miner", 
		version     = 0.1, 
		description = "AIO radius based miner", 
		gameMode    = 1)

public class FCAIOMiner extends Script implements Starting, Ending, Painting, FCPaintable, FCInventoryListener, Pausing
{	
	//Local constants
	private final ScriptManifest 		MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class); //The script manifest object
	private final FCInventoryObserver 	INV_OBSERVER = new FCInventoryObserver();	//This is how we'll observe our ores being added to inv
	private final GUI					GUI = new GUI(this);
	private final ACamera				ACAMERA	= new ACamera(); //For asynchronous camera movement
	private final int					SLEEP_TIME = 100; //Sleep time between cycles, reduces CPU usage
	
	//Public constants
	public final MinerTaskManager		TASK_MANAGER = new MinerTaskManager(this, ACAMERA); //Our task manager
	
	//Local variables
		
	@Override
	public void run()
	{
		while(Vars.get().isRunning) //WHILE(our script is running --> this variable can be set to false from anywhere and the script will stop)
		{
			sleep(SLEEP_TIME); //sleep to reduce CPU usage
			
			if(Vars.get().needGuiEntry) //IF(we still need to fill out the GUI)
			{
				continue; //skip this loop iteration
			}
			
			updateStartingXp(); //set the starting xp / level if necessary
			
			TASK_MANAGER.executeTasks(); //execute the tasks (main script logic is in each task class)
		}
	}
	
	@Override
	public String[] getPaintInfo()
	{
		/*
		 * These variables are here because the info they hold is used more than 1 time throughout this method
		 */
		int xpGained = Skills.getXP(SKILLS.MINING) - Vars.get().startingXp;
		int level = Skills.getActualLevel(SKILLS.MINING);
		Task currentTask = TASK_MANAGER.getCurrentTask();
		String task = currentTask == null || currentTask.getStatus() == null ? "None" : currentTask.getStatus();
		
		return new String[]{MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0], 
				"Time ran: " + Vars.get().paint.getTimeRan(), "Mode: " + Vars.get().mode, "Current Task: " + task,
				"Current level (gained): " + level + " (" + (level - Vars.get().startingLevel) + ")",
				"Ore mined (p/h): " + Vars.get().oresMined + " (" + Vars.get().paint.getPerHour(Vars.get().oresMined) + ")",
				"Ore stolen (p/h): " + Vars.get().oresStolen + " (" + Vars.get().paint.getPerHour(Vars.get().oresStolen) + ")",
				"XP gained (p/h): " + xpGained + " (" + Vars.get().paint.getPerHour(xpGained) + ")", 
				"Times hopped (p/h): " + Vars.get().timesHopped + " (" + Vars.get().paint.getPerHour(Vars.get().timesHopped) + ")"};
	}

	@Override
	public void onPaint(Graphics g)
	{
		if(!Vars.get().needGuiEntry) //IF(the gui has been filled out)
		{
			Vars.get().paint.paint(g); //Draw the paint on the screen
		}
	}

	@Override
	public void onEnd()
	{
		println("Thank you for running " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
	}

	@Override
	public void onStart()
	{
		Vars.reset();
		
		Vars.get().paint = new FCPaint(this);
		
		if(Vars.get().isUsingAbc) //IF(the user selected ABCL10 in the GUI)
		{
			General.useAntiBanCompliance(true); //Force ABC usage of some API methods
		}
		
		INV_OBSERVER.addListener(this);
		
		println("Started " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
		
		Utils.handleGui(GUI); //Start the GUI
	}
	
	private void updateStartingXp()
	{
		if(Vars.get().startingXp == -1 && Login.getLoginState() == STATE.INGAME)
		{
			Vars.get().startingXp = Skills.getXP(SKILLS.MINING);
			
			Vars.get().startingLevel = Skills.getActualLevel(SKILLS.MINING);
		}
	}

	@Override
	public void inventoryAdded(int id, int count)
	{
		if(Vars.get().rockType != null && Vars.get().rockType.getItemId() == id)
		{
			Vars.get().oresMined += count;
		}
	}

	@Override
	public void inventoryRemoved(int id, int count)
	{}

	@Override
	public void onPause()
	{
		Vars.get().paint.pause();
	}

	@Override
	public void onResume()
	{
		Vars.get().paint.resume();
	}
}
