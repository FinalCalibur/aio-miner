package scripts.fc.fcaiominer.data;

/**
 * Mode - This enum represents which dropping mode the user has chosen
 * 
 * @author Freddy
 *
 */
public enum Mode
{
	BANK_ORE,
	DROP_INVENTORY,
	M1D1;
}
