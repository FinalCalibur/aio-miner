package scripts.fc.fcaiominer.data;

import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSTile;
import org.tribot.api2007.util.DPathNavigator;

import scripts.fc.fcpaint.FCPaint;

public class Vars 
{
	private static Vars vars;

	public static Vars get()
	{
		return vars == null ? vars = new Vars() : vars;
	}
	
	public static void reset() 
	{
		vars = null;
	}
	
	public FCPaint		paint;
	public boolean 		isRunning = true;
	public boolean		needGuiEntry = true;
	public boolean		isCurrentlyUpgradingPick;
	public boolean		isUsingAbc;
	public ABCUtil 		abc = new ABCUtil();
	public RockType 	rockType;
	public Pickaxe		pickAxe;
	public Mode			mode;
	public boolean		isUpgradingPick;
	public RSItem[]		bankedPicks;
	public RSTile[] 	customPath;
	public boolean		isWorldHopping;
	public int 			maxPlayersInArea;
	public int			maxOresStolenPerHour;
	public int			minOresGainedPerHour;
	public int 			oresMined;
	public int			oresStolen;
	public int			startingXp = -1;
	public int			timesHopped;
	public int 			startingLevel = -1;
	public RSTile		miningAreaTopLeft;
	public RSTile		miningAreaBottomRight;
	public RSArea		miningArea;
	public DPathNavigator dPath = new DPathNavigator();
}
