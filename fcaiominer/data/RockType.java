package scripts.fc.fcaiominer.data;

import java.util.HashSet;

/**
 *	RockType - This enum holds information about the various mining rock types
 *		
 *		It holds the color of the rock, found using RSObjectDefinition#getModifiedColors
 *			(so we can find the rocks, and cache the ids)
 *
 *		It also holds the item ID of the ore (unnoted)
 *		
 *	@author Freddy
 *
 */
public enum RockType
{
	CLAY(6705, 434),
	COPPER(4645, 436),
	TIN(53, 438),
	IRON(2576, 440),
	SILVER(7366, 442),
	COAL(7580, 453),
	GOLD(8885, 444),
	MITHRIL(-22239, 447),
	ADAMANTITE(21662, 449),
	RUNITE(-31437, 451);
	
	private int color;
	private int itemId;
	private HashSet<Integer> ids = new HashSet<Integer>(); //The ids hashset contains all of the found object ids for our target rocks
	
	RockType(int color, int itemId)
	{
		this.color = color;
		this.itemId = itemId;
	}
	
	public int getColor()
	{
		return color;
	}
	
	public int getItemId()
	{
		return itemId;
	}
	
	public HashSet<Integer> getIds()
	{
		return ids;
	}

}
