package scripts.fc.fcaiominer.gui;

import javax.swing.DefaultListModel;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.fcaiominer.FCAIOMiner;
import scripts.fc.fcaiominer.data.Mode;
import scripts.fc.fcaiominer.data.RockType;
import scripts.fc.fcaiominer.data.Vars;

/**
 *
 * @author Final Calibur
 */
public class GUI extends javax.swing.JFrame
{
	private static final long serialVersionUID = 4910058314305379555L;
	
	private FCAIOMiner script;
	
	public GUI(FCAIOMiner script)
	{
		this.script = script;
		
		initComponents();
		
		customPathList.setModel(customPath);
	}

	
	private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        generalTab = new javax.swing.JPanel();
        rockTypeLabel = new javax.swing.JLabel();
        modeLabel = new javax.swing.JLabel();
        modeBox = new javax.swing.JComboBox<Mode>();
        radiusLabel = new javax.swing.JLabel();
        radiusSpinner = new javax.swing.JSpinner();
        pickUpgradingBox = new javax.swing.JCheckBox();
        pickUpgradingBox.setSelected(true);
        pickUpgradingLabel = new javax.swing.JLabel();
        rockTypeBox = new javax.swing.JComboBox<RockType>();
        startButton = new javax.swing.JButton();
        abclLabel = new javax.swing.JLabel();
        abclCheckBox = new javax.swing.JCheckBox();
        abclCheckBox.setSelected(true);
        customPathTab = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        customPathList = new javax.swing.JList<RSTile>();
        addTileButton = new javax.swing.JButton();
        removeTileButton = new javax.swing.JButton();
        clearPathButton = new javax.swing.JButton();
        customPathLabel = new javax.swing.JLabel();
        worldHoppingTab = new javax.swing.JPanel();
        hoppingEnabledBox = new javax.swing.JCheckBox();
        hopWorldsLabel = new javax.swing.JLabel();
        hopLabel1 = new javax.swing.JLabel();
        playersInAreaSpinner = new javax.swing.JSpinner();
        hopLabel2 = new javax.swing.JLabel();
        oresStolenSpinner = new javax.swing.JSpinner();
        hopLabel3 = new javax.swing.JLabel();
        oresGainedSpinner = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("FC AIO Miner by Final Calibur");
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        titleLabel.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        titleLabel.setText("FC AIO Miner");

        rockTypeLabel.setText("Rock Type:");

        modeLabel.setText("Mode:");

        modeBox.setModel(new javax.swing.DefaultComboBoxModel<Mode>(Mode.values()));

        radiusLabel.setText("Radius:");

        radiusSpinner.setModel(new javax.swing.SpinnerNumberModel(5, 1, 40, 1));
        radiusSpinner.setToolTipText("");

        pickUpgradingBox.setText("Enabled");

        pickUpgradingLabel.setText("Pick Upgrading:");

        rockTypeBox.setModel(new javax.swing.DefaultComboBoxModel<RockType>(RockType.values()));

        startButton.setText("Start");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        abclLabel.setText("ABCL 10");

        abclCheckBox.setText("Enabled");

        javax.swing.GroupLayout generalTabLayout = new javax.swing.GroupLayout(generalTab);
        generalTab.setLayout(generalTabLayout);
        generalTabLayout.setHorizontalGroup(
            generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalTabLayout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, generalTabLayout.createSequentialGroup()
                .addContainerGap(71, Short.MAX_VALUE)
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rockTypeLabel)
                    .addComponent(radiusLabel)
                    .addComponent(modeLabel)
                    .addComponent(pickUpgradingLabel)
                    .addComponent(abclLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(abclCheckBox)
                    .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(modeBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(radiusSpinner)
                        .addComponent(rockTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pickUpgradingBox, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(50, 50, 50))
        );
        generalTabLayout.setVerticalGroup(
            generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rockTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rockTypeLabel))
                .addGap(6, 6, 6)
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radiusLabel)
                    .addComponent(radiusSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modeLabel)
                    .addComponent(modeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pickUpgradingBox)
                    .addComponent(pickUpgradingLabel))
                .addGap(4, 4, 4)
                .addGroup(generalTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(abclLabel)
                    .addComponent(abclCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(startButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("General", generalTab);

        jScrollPane1.setViewportView(customPathList);

        addTileButton.setText("Add tile");
        addTileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTileButtonActionPerformed(evt);
            }
        });

        removeTileButton.setText("Remove tile");
        removeTileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeTileButtonActionPerformed(evt);
            }
        });

        clearPathButton.setText("Clear path");
        clearPathButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearPathButtonActionPerformed(evt);
            }
        });

        customPathLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        customPathLabel.setText(" Path from rocks to bank");

        javax.swing.GroupLayout customPathTabLayout = new javax.swing.GroupLayout(customPathTab);
        customPathTab.setLayout(customPathTabLayout);
        customPathTabLayout.setHorizontalGroup(
            customPathTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, customPathTabLayout.createSequentialGroup()
                .addGroup(customPathTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(customPathLabel)
                    .addGroup(customPathTabLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(customPathTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(clearPathButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(removeTileButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addTileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
        );
        customPathTabLayout.setVerticalGroup(
            customPathTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(customPathTabLayout.createSequentialGroup()
                .addComponent(customPathLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addTileButton)
                .addGap(18, 18, 18)
                .addComponent(removeTileButton)
                .addGap(18, 18, 18)
                .addComponent(clearPathButton)
                .addGap(23, 23, 23))
        );

        jTabbedPane1.addTab("Custom Pathing", customPathTab);

        hoppingEnabledBox.setText("Enabled");
        hoppingEnabledBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hoppingEnabledBoxActionPerformed(evt);
            }
        });

        hopWorldsLabel.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        hopWorldsLabel.setText("Hop worlds if:");
        hopWorldsLabel.setEnabled(false);

        hopLabel1.setText("Other players in area     >=");
        hopLabel1.setEnabled(false);

        playersInAreaSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(5), Integer.valueOf(1), null, Integer.valueOf(1)));
        playersInAreaSpinner.setEnabled(false);

        hopLabel2.setText("Ores stolen per hour     >=");
        hopLabel2.setEnabled(false);

        oresStolenSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(5), Integer.valueOf(1), null, Integer.valueOf(1)));
        oresStolenSpinner.setEnabled(false);

        hopLabel3.setText("Ores gained per hour    <=");
        hopLabel3.setEnabled(false);

        oresGainedSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(5), Integer.valueOf(1), null, Integer.valueOf(1)));
        oresGainedSpinner.setEnabled(false);

        javax.swing.GroupLayout worldHoppingTabLayout = new javax.swing.GroupLayout(worldHoppingTab);
        worldHoppingTab.setLayout(worldHoppingTabLayout);
        worldHoppingTabLayout.setHorizontalGroup(
            worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(worldHoppingTabLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hopLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(hopLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(hopLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(oresStolenSpinner, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
                    .addComponent(playersInAreaSpinner, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(oresGainedSpinner))
                .addGap(30, 30, 30))
            .addGroup(worldHoppingTabLayout.createSequentialGroup()
                .addGap(105, 105, 105)
                .addGroup(worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hopWorldsLabel)
                    .addGroup(worldHoppingTabLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(hoppingEnabledBox)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        worldHoppingTabLayout.setVerticalGroup(
            worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(worldHoppingTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(hoppingEnabledBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(hopWorldsLabel)
                .addGap(18, 18, 18)
                .addGroup(worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hopLabel1)
                    .addComponent(playersInAreaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hopLabel2)
                    .addComponent(oresStolenSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(worldHoppingTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hopLabel3)
                    .addComponent(oresGainedSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("World Hopping", worldHoppingTab);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addGroup(layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addComponent(titleLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1))
        );

        pack();
    }

	private void startButtonActionPerformed(java.awt.event.ActionEvent evt)
	{
		Vars.get().rockType = (RockType)rockTypeBox.getSelectedItem();
		Vars.get().mode = (Mode)modeBox.getSelectedItem();
		Vars.get().isUpgradingPick = pickUpgradingBox.isSelected();
		Vars.get().customPath = new RSTile[customPath.size()];
		customPath.copyInto(Vars.get().customPath);
		Vars.get().isWorldHopping = hoppingEnabledBox.isSelected();
		Vars.get().maxPlayersInArea = (int)playersInAreaSpinner.getValue();
		Vars.get().maxOresStolenPerHour = (int)oresStolenSpinner.getValue();
		Vars.get().minOresGainedPerHour = (int)oresGainedSpinner.getValue();
		Vars.get().miningAreaTopLeft = Player.getPosition().translate(-(int)radiusSpinner.getValue(), (int)radiusSpinner.getValue());
		Vars.get().miningAreaBottomRight = Player.getPosition().translate((int)radiusSpinner.getValue(), -(int)radiusSpinner.getValue());
		Vars.get().miningArea = new RSArea(Vars.get().miningAreaTopLeft, Vars.get().miningAreaBottomRight);
		script.TASK_MANAGER.addAppropriateTasks();
		Vars.get().isUsingAbc = abclCheckBox.isSelected();
		Vars.get().needGuiEntry = false;
		this.dispose();
	}

	private void addTileButtonActionPerformed(java.awt.event.ActionEvent evt)
	{
		if(!customPath.contains(Player.getPosition()))
		{
			customPath.addElement(Player.getPosition());
		}
	}

	private void removeTileButtonActionPerformed(java.awt.event.ActionEvent evt)
	{
		customPath.remove(customPathList.getSelectedIndex());
	}

	private void clearPathButtonActionPerformed(java.awt.event.ActionEvent evt)
	{
		customPath.clear();
	}

	private void hoppingEnabledBoxActionPerformed(java.awt.event.ActionEvent evt)
	{
		setHoppingTab(hoppingEnabledBox.isSelected());
	}
	
	private void setHoppingTab(boolean enabled)
	{
		hopLabel1.setEnabled(enabled);
		hopLabel2.setEnabled(enabled);
		hopLabel3.setEnabled(enabled);
		hopWorldsLabel.setEnabled(enabled);
		playersInAreaSpinner.setEnabled(enabled);
		oresGainedSpinner.setEnabled(enabled);
		oresStolenSpinner.setEnabled(enabled);
	}

	// Variables declaration - do not modify
	private javax.swing.JCheckBox abclCheckBox;
    private javax.swing.JLabel abclLabel;
	private javax.swing.JButton addTileButton;
	private javax.swing.JButton clearPathButton;
	private javax.swing.JLabel customPathLabel;
	private javax.swing.JList<RSTile> customPathList;
	private DefaultListModel<RSTile> customPath = new DefaultListModel<RSTile>();
	private javax.swing.JPanel customPathTab;
	private javax.swing.JPanel generalTab;
	private javax.swing.JLabel hopLabel2;
	private javax.swing.JLabel hopLabel3;
	private javax.swing.JLabel hopLabel1;
	private javax.swing.JLabel hopWorldsLabel;
	private javax.swing.JCheckBox hoppingEnabledBox;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JComboBox<Mode> modeBox;
	private javax.swing.JLabel modeLabel;
	private javax.swing.JSpinner oresGainedSpinner;
	private javax.swing.JSpinner oresStolenSpinner;
	private javax.swing.JCheckBox pickUpgradingBox;
	private javax.swing.JLabel pickUpgradingLabel;
	private javax.swing.JSpinner playersInAreaSpinner;
	private javax.swing.JLabel radiusLabel;
	private javax.swing.JSpinner radiusSpinner;
	private javax.swing.JButton removeTileButton;
	private javax.swing.JComboBox<RockType> rockTypeBox;
	private javax.swing.JLabel rockTypeLabel;
	private javax.swing.JButton startButton;
	private javax.swing.JLabel titleLabel;
	private javax.swing.JPanel worldHoppingTab;
	// End of variables declaration
}
