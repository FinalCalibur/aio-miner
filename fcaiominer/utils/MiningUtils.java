package scripts.fc.fcaiominer.utils;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;

import scripts.fc.fcaiominer.data.Pickaxe;

public class MiningUtils
{
	public static Pickaxe getBestUsablePick(boolean isCheckingBank)
	{
		Pickaxe bestPick = null;
		
		for(Pickaxe pick : Pickaxe.values())
		{
			if(pick.getMiningLevel() <= Skills.getActualLevel(SKILLS.MINING) && ((Inventory.getCount(pick.getItemId()) > 0 
					|| Equipment.getCount(pick.getItemId()) > 0) || (isCheckingBank && Banking.find(pick.getItemId()).length > 0)))
			{
				bestPick = pick;
			}
		}
		
		return bestPick;
	}
}
