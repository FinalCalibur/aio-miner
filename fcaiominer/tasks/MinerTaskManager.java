package scripts.fc.fcaiominer.tasks;

import java.util.LinkedList;

import org.tribot.api.General;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.fcaiominer.tasks.impl.BankOre;
import scripts.fc.fcaiominer.tasks.impl.DropInventory;
import scripts.fc.fcaiominer.tasks.impl.EquipPick;
import scripts.fc.fcaiominer.tasks.impl.M1D1;
import scripts.fc.fcaiominer.tasks.impl.MineOre;
import scripts.fc.fcaiominer.tasks.impl.PickAxeCheck;
import scripts.fc.fcaiominer.tasks.impl.UpgradePick;
import scripts.fc.fcaiominer.tasks.impl.WorldHop;
import scripts.fc.framework.Task;
import scripts.fc.framework.TaskManager;

public class MinerTaskManager extends TaskManager
{

	public MinerTaskManager(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public LinkedList<Task> getTaskList()
	{
		LinkedList<Task> tasks = new LinkedList<Task>();	
		
		tasks.add(new PickAxeCheck(script, aCamera));
		
		tasks.add(new EquipPick(script, aCamera));
		
		return tasks;
	}
	
	public void addAppropriateTasks()
	{
		switch(Vars.get().mode)
		{
			case BANK_ORE:
				tasks.add(new BankOre(script, aCamera));
			break;
			
			case DROP_INVENTORY:
				tasks.add(new DropInventory(script, aCamera));
			break;
			
			default:
				tasks.add(new M1D1(script, aCamera));
		}
		
		if(Vars.get().isUpgradingPick)
		{
			tasks.add(new UpgradePick(script, aCamera));
		}
		
		if(Vars.get().isWorldHopping)
		{
			tasks.add(new WorldHop(script, aCamera));
		}
		
		tasks.add(new MineOre(script, aCamera));
		
		General.println("Added appropriate tasks for your goals");
	}

}
