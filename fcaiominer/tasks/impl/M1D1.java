package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.ChooseOption;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.api.FCConditions;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.framework.Task;

public class M1D1 extends Task
{
	public static final int DROPPING_TRIGGER = General.random(6, 10); //Inventory length we should drop at
	
	public M1D1(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{	
		/*
		 * The MineOre task handles preparing the drop, which means the ChooseOption
		 * menu should be open at this point, and we should be able to drop the ore.
		 */
		if(ChooseOption.select("Drop")) //IF(we have successfully selected the drop option on the menu)
		{	
			//Wait until inventory has changed
			Timing.waitCondition(FCConditions.inventoryChanged(Inventory.getAll().length), General.random(1500, 3000));
		}
		else //ELSE(we could not select the drop option on the menu for whatever reason)
		{
			/*
			 * Find and click drop on an ore in the inventory
			 */
			RSItem[] items = Inventory.find(Vars.get().rockType.getItemId());
			
			if(items.length > 0)
			{
				items[0].click("Drop");
			}
		}
	}

	@Override
	public boolean shouldExecute()
	{
		return Inventory.getAll().length > DROPPING_TRIGGER && !Vars.get().isCurrentlyUpgradingPick;
	}

	@Override
	public String getStatus()
	{
		return "M1D1";
	}

}
