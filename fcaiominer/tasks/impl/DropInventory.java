package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.General;
import org.tribot.api2007.Inventory;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.framework.Task;

public class DropInventory extends Task
{

	public DropInventory(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		General.println("Dropping inventory");
		
		Inventory.dropAllExcept(Vars.get().pickAxe.getItemId());
	}

	@Override
	public boolean shouldExecute()
	{
		return Inventory.isFull();
	}

	@Override
	public String getStatus()
	{
		return "Drop inventory";
	}

}
