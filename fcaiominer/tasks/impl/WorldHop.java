package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Players;
import org.tribot.api2007.WorldHopper;
import org.tribot.api2007.ext.Filters;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.fcaiominer.utils.Utils;
import scripts.fc.framework.Task;

public class WorldHop extends Task
{
	private final long MIN_TIME_ON_WORLD = 900000; //15 minutes
	
	private long worldStart = Timing.currentTimeMillis(); //Time we started on the current world
	
	public WorldHop(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		General.println("World hop");
		
		if(Login.getLoginState() == STATE.INGAME)
		{
			Login.logout();
		}
		
		if(WorldHopper.changeWorld(WorldHopper.getRandomWorld(Utils.isMember())))
		{
			worldStart = Timing.currentTimeMillis();
			
			Vars.get().timesHopped++;
			
			Vars.get().oresStolen = 0;
			
			Vars.get().oresMined = 0;
		}
	}

	@Override
	public boolean shouldExecute()
	{
		if(Vars.get().paint == null)
		{
			return false;
		}
		
		long stolenPerHour = Vars.get().paint.getPerHour(Vars.get().oresStolen);
		long gainedPerHour = Vars.get().paint.getPerHour(Vars.get().oresMined);
		
		return Login.getLoginState() == STATE.INGAME && Vars.get().isWorldHopping && 
				(playersInArea() >= Vars.get().maxPlayersInArea ||
				(Timing.timeFromMark(worldStart) > MIN_TIME_ON_WORLD &&
				(stolenPerHour >= Vars.get().maxOresStolenPerHour ||
				gainedPerHour <= Vars.get().minOresGainedPerHour)));
	}

	@Override
	public String getStatus()
	{
		return "World hop";
	}
	
	private int playersInArea()
	{
		return Players.find(Filters.Players.inArea(Vars.get().miningArea)).length;
	}

}
