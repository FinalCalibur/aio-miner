package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.Clicking;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSItem;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.framework.Task;

public class EquipPick extends Task
{
	private boolean isImpossible;
	
	public EquipPick(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		/*
		 * We check if it's impossible to equip our current pick. If so, it also means it is impossible
		 * to equip any future pick as well. If this results to true, this task will no longer be run. We could
		 * remove it from the task manager, but this works just as well.
		 */
		isImpossible = Skills.getActualLevel(SKILLS.ATTACK) < Vars.get().pickAxe.getAttackLevel() ? true : false;
		
		if(!isImpossible) //IF(we can wield our current pick)
		{
			RSItem[] picks = Inventory.find(Vars.get().pickAxe.getItemId()); //Find current pick in inventory
			
			if(picks.length > 0)
			{
				Clicking.click("Wield", picks[0]); //Wield it
			}
		}
	}

	@Override
	public boolean shouldExecute()
	{
		/*
		 * Should only execute this task if we have determined that it is not impossible to equip our pick
		 * or any future picks. Then we check if we don't have the pick currently equipped. And finally we
		 * check if we have the pick in our inventory. We could probably get away with simply checking if the
		 * pick is in our inventory (since that means it isn't equipped), but if the player somehow has two of
		 * the same pick in the inventory, it will be an infinite loop of wielding pickaxes.
		 */
		return !isImpossible && !Equipment.isEquipped(Vars.get().pickAxe.getItemId())
				&& Inventory.getCount(Vars.get().pickAxe.getItemId()) > 0;
	}

	@Override
	public String getStatus()
	{
		return "Equip pick";
	}

}
