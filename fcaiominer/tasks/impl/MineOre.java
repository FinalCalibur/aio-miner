package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api.util.Sorting;
import org.tribot.api2007.ChooseOption;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSMenuNode;
import org.tribot.api2007.types.RSObject;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.api.FCConditions;
import scripts.fc.fcaiominer.data.Mode;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.fcaiominer.utils.Utils;
import scripts.fc.framework.Task;

public class MineOre extends Task
{
	private final Filter<RSObject> ROCK_FILTER = rockFilter(); //Filter we'll use to find our rocks
	private final Condition MINING_CONDITION = miningCondition(); //Condition to tell us whether or not we're mining
	
	private RSObject targetRock; //The rock that we are currently targetting
	private RSObject updatedTargetRock; //This variable holds the updated RSObject of our target rock (so we can compare ids)
	private long lastBusyTime; //This is used for ABC --> It tells us when we were last busy (mining)
	private boolean sentHoverMessage;
	
	public MineOre(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		if(!isInLocation())
		{
			goToLocation();
		}
		else if(!isMining(true))
		{
			if(Vars.get().isUsingAbc)
			{
				resetAbc();
			
				Vars.get().abc.waitNewOrSwitchDelay(lastBusyTime, false); //ABCL switch / new object delay
			}
			
			mineOre();
		}
		else
		{	
			lastBusyTime = Timing.currentTimeMillis(); //This is for ABCL switch object delay
			
			doActionsWhileMining();
		}
	}

	@Override
	public boolean shouldExecute()
	{
		return !Inventory.isFull() && !Vars.get().isCurrentlyUpgradingPick;
	}

	@Override
	public String getStatus()
	{
		return "Mine " + Vars.get().rockType.name().toLowerCase();
	}

	private void mineOre()
	{
		findTargetRock();
		
		if(targetRock != null)
		{
			if(!targetRock.isClickable())
			{
				aCamera.turnToTile(targetRock);
				
				Walking.blindWalkTo(targetRock);
			}
			else 
			{	
				if(DynamicClicking.clickRSObject(targetRock, "Mine"))
				{
					Timing.waitCondition(MINING_CONDITION, General.random(3000, 5000));
				}
			}
		}
		else
		{
			lastBusyTime = Timing.currentTimeMillis(); //This is for ABCL new object delay
		}
	}
	
	private void findTargetRock()
	{	
		RSObject[] potentialRocks = getPotentialRocks();
		
		if(potentialRocks.length > 0)
		{	
			targetRock = potentialRocks.length > 1 && Vars.get().isUsingAbc && Vars.get().abc.BOOL_TRACKER.USE_CLOSEST.next()
					? potentialRocks[1] : potentialRocks[0];
		}
		else
		{
			targetRock = null;
		}
	}
	
	private void hoverNextRock()
	{
		RSObject[] potentialRocks = getPotentialRocks();
		
		if(!sentHoverMessage)
		{
			General.println("ABC hover next");
			
			sentHoverMessage = true;
		}
		
		if(potentialRocks.length > 1)
		{
			potentialRocks[1].hover();
		}
	}
	
	private RSObject[] getPotentialRocks()
	{
		RSObject[] potentialRocks = Objects.getAllIn(Vars.get().miningAreaTopLeft, Vars.get().miningAreaBottomRight, ROCK_FILTER);
		
		Sorting.sortByDistance(potentialRocks, Player.getPosition(), true);
		
		return potentialRocks;
	}
	
	private void doActionsWhileMining()
	{
		if(Vars.get().mode == Mode.M1D1 && Inventory.getAll().length >= M1D1.DROPPING_TRIGGER && !ChooseOption.isOpen())
		{
			prepareOreDrop();
		}
		else if(Vars.get().mode != Mode.M1D1 && Vars.get().isUsingAbc)
		{
			if(Vars.get().abc.performTimedActions(SKILLS.MINING))
			{
				General.println("ABC antiban");
			}
			else if(Vars.get().abc.BOOL_TRACKER.HOVER_NEXT.next())
			{
				hoverNextRock();
			}
		}
	}
	
	private boolean isMining(boolean checkIfStolen)
	{	
		updatedTargetRock = getUpdatedTargetRock();
		
		return Player.getAnimation() != -1 && (!checkIfStolen || !isOreStolen());
	}
	
	private boolean isOreStolen()
	{
		if(updatedTargetRock != null && !Vars.get().rockType.getIds().contains(updatedTargetRock.getID()))
		{
			Vars.get().oresStolen++;
			
			return true;
		}
		
		return false;
	}
	
	private RSObject getUpdatedTargetRock()
	{
		if(targetRock != null)
		{
			RSObject[] rocks = Objects.getAt(targetRock);
			
			if(rocks.length > 0)
			{
				return rocks[0];
			}
		}
		
		return null;
	}
	
	private boolean isInLocation()
	{
		return Vars.get().miningArea.contains(Player.getPosition());
	}
	
	private void prepareOreDrop()
	{
		RSItem[] ore = Inventory.find(Filters.Items.idNotEquals(Vars.get().pickAxe.getItemId()));
		
		if(ore.length > 0)
		{
			RSItem toDrop = ore[0];
			
			if(toDrop.hover())
			{
				Mouse.click(3);
				
				RSMenuNode node = Utils.getOption("Drop");
				
				if(node != null)
				{
					Mouse.moveBox(node.getArea());
				}
			}
		}
	}
	
	private void goToLocation()
	{
		if(Vars.get().customPath.length > 0)
		{
			Walking.walkPath(Walking.invertPath(Vars.get().customPath));
		}
		else
		{
			WebWalking.walkTo(Vars.get().miningArea.getRandomTile(), FCConditions.inAreaCondition(Vars.get().miningArea), 100);
		}
	}
	
	private Filter<RSObject> rockFilter()
	{
		return new Filter<RSObject>()
		{
			@Override
			public boolean accept(RSObject o)
			{
				if(Vars.get().rockType.getIds().contains(o.getID()))
				{
					return true;
				}
				else if(isColorMatch(o))
				{
					Vars.get().rockType.getIds().add(o.getID());
					
					return true;
				}
				
				return false;
			}
			
		}.combine(Filters.Objects.nameEquals("Rocks"), true);
	}
	
	private boolean isColorMatch(RSObject o)
	{
		for(short color : o.getDefinition().getModifiedColors())
		{
			if(color == Vars.get().rockType.getColor())
			{
				return true;
			}
		}
		
		return false;
	}
	
	private Condition miningCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				
				return isMining(false) || isOreStolen();
			}	
		};
	}
	
	private void resetAbc()
	{
		Vars.get().abc.BOOL_TRACKER.USE_CLOSEST.reset();
		
		Vars.get().abc.BOOL_TRACKER.HOVER_NEXT.reset();
		
		sentHoverMessage = false;
	}

}
