package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.fcaiominer.utils.MiningUtils;
import scripts.fc.framework.Task;

public class PickAxeCheck extends Task
{

	public PickAxeCheck(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		General.println("Pickaxe check");
		
		Vars.get().pickAxe = MiningUtils.getBestUsablePick(Banking.isBankScreenOpen());
	}

	@Override
	public boolean shouldExecute()
	{
		return Vars.get().pickAxe == null;
	}

	@Override
	public String getStatus()
	{
		return "Pickaxe check";
	}

}
