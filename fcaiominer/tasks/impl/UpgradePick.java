package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.fcaiominer.data.Mode;
import scripts.fc.fcaiominer.data.Vars;

public class UpgradePick extends BankOre
{

	public UpgradePick(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		/*
		 * We set this variable to true so that the BankOre task knows
		 * to withdraw the pick (since we're using it's methods and extending the class)
		 */
		Vars.get().isCurrentlyUpgradingPick = true;
		
		//Call upon BankOre's execute method
		super.execute();
	}

	@Override
	public boolean shouldExecute()
	{
		return Vars.get().isUpgradingPick && needsPickUpgrade() && 
				((Vars.get().bankedPicks == null && Vars.get().mode != Mode.BANK_ORE) || hasPickUpgrade());
	}

	@Override
	public String getStatus()
	{
		return "Upgrade Pick";
	}

}
