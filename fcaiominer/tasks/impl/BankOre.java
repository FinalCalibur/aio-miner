package scripts.fc.fcaiominer.tasks.impl;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Constants;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSItem;
import org.tribot.script.Script;

import scripts.fc.api.ACamera;
import scripts.fc.api.FCConditions;
import scripts.fc.fcaiominer.data.Pickaxe;
import scripts.fc.fcaiominer.data.Vars;
import scripts.fc.fcaiominer.utils.MiningUtils;
import scripts.fc.framework.Task;

public class BankOre extends Task
{
	public BankOre(Script script, ACamera aCamera)
	{
		super(script, aCamera);
	}

	@Override
	public void execute()
	{
		//Pretty self explanatory logic here, very verbose
		if(!Banking.isInBank())
		{
			goToBank();
		}
		else
		{
			bankOres();
		}
	}

	@Override
	public boolean shouldExecute()
	{
		/*
		 * We should execute this task when our inventory is full. Can't keep mining
		 * if we have no inventory space.
		 */
		return Inventory.isFull();
	}

	@Override
	public String getStatus()
	{
		return "Bank ore";
	}
	
	protected void goToBank()
	{
		if(Vars.get().customPath.length > 0) //IF(user created a custom path)
		{
			General.println("Walking custom path");
			
			Walking.walkPath(Vars.get().customPath); //Walk the custom path to the bank
		}
		else //ELSE(user has not created a custom path)
		{
			if(WebWalking.walkToBank()) //IF(we successfully webwalked to the bank)
			{
				//Wait until player is actually in the bank - Web walking sometimes likes to return true before we're in the bank
				Timing.waitCondition(FCConditions.IN_BANK_CONDITION, General.random(3000, 5000));
			}
		}
	}
	
	/*
	 * This method is a little long because I was lazy and threw pick upgrading
	 * in there with it. Generally not a good thing to do, each method should
	 * have one specific task
	 */
	protected void bankOres()
	{
		if(!Banking.isBankScreenOpen()) //IF(bank screen is NOT open)
		{
			Banking.openBank(); //Open the bank
		}
		else //ELSE(bank screen is open)
		{	
			Vars.get().bankedPicks = Banking.find(Constants.IDs.Items.pickaxes); //Populate bankedPicks array
			
			//IF(user chose pick upgrading AND we need a pick upgrade AND we have a pick upgrade)
			if(Vars.get().isUpgradingPick && needsPickUpgrade() && hasPickUpgrade())
			{
				Banking.depositAll(); //Deposit inventory
				
				General.sleep(General.randomSD(100, 800, 400, 200)); //Little sleep before we withdraw our pick upgrade
				
				Banking.withdraw(1, MiningUtils.getBestUsablePick(true).getItemId()); //Withdraw pick upgrade
				
				Vars.get().pickAxe = null; //Set current pick to null -> so UpgradePick task gets called
			}
			else //ELSE(we do not need to upgrade our pick atm for whatever reason)
			{
				Banking.depositAllExcept(Vars.get().pickAxe.getItemId()); //Deposit all items except pick
				
				General.sleep(General.randomSD(100, 1500, 600, 300)); //Little sleep after we deposit our items
			}
			
			Vars.get().isCurrentlyUpgradingPick = false;
		}
	}
	
	protected boolean needsPickUpgrade()
	{
		/*
		 * This pretty much checks first if you already have the best pick.
		 * 
		 * It then checks if you're mining level is >= to the next pickaxe after your current one
		 */
		return Vars.get().pickAxe != null && Vars.get().pickAxe.ordinal() != Pickaxe.values().length - 1
				&& Skills.getActualLevel(SKILLS.MINING) >= Pickaxe.values()[Vars.get().pickAxe.ordinal() + 1].getMiningLevel();
	}
	
	protected boolean hasPickUpgrade()
	{
		
		//IF(our bankedPicks array has not been populated yet or we have not set our pickaxe)
		if(Vars.get().bankedPicks == null || Vars.get().pickAxe == null)
		{
			return false;
		}
		
		for(RSItem pick : Vars.get().bankedPicks) //FOR(each pick in our bankedPicks array)
		{
			//IF(the pick has a greater mining requirement than our current pick)
			if(Pickaxe.get(pick.getID()).getMiningLevel() > Vars.get().pickAxe.getMiningLevel())
			{
				return true;
			}
		}
		
		return false;
	}
}
